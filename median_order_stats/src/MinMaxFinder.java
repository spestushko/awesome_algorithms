public class MinMaxFinder {

    static int[] find(int[] a) {
        int min = a[0];
        int max = a[0];

        for (int i = 1; i < a.length; ++i) {
            min = Math.min(a[i], min);
            max = Math.max(a[i], max);
        }

        return new int[]{min, max};
    }

}
