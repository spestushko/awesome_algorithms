class RandomizedSelect {

    static int find(int[] a, int p, int r, int i) {
        if (p == r)
            return a[p];
        int q = RandomQuickSort.randomPartition(a, p, r);
        int k = q - p + 1;
        if (i == k)
            return a[q];
        else if (i < k)
            return find(a, p, q - 1, i);
        else
            return find(a, q + 1, r, i - k);
    }

}
