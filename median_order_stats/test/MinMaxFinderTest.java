import org.junit.Test;
import org.junit.Assert;

public class MinMaxFinderTest {

    @Test
    public void testFindingMinMax() {
        // Arrange
        int[] a = new int[]{6, 4, 2, 9, 1, 8, 5, 3, 7};
        int[] expectedMinMax = new int[]{1, 9};
        // Act
        int[] actualMinMax = MinMaxFinder.find(a);
        // Assert
        Assert.assertArrayEquals(expectedMinMax, actualMinMax);
    }

    @Test
    public void testFindingMinMaxSame() {
        // Arrange
        int[] a = new int[]{1, 1, 1, 1, 1, 1, 1};
        int[] expectedMinMax = new int[]{1, 1};
        // Act
        int[] actualMinMax = MinMaxFinder.find(a);
        // Assert
        Assert.assertArrayEquals(expectedMinMax, actualMinMax);
    }
}
