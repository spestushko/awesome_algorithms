import org.junit.Test;
import org.junit.Assert;

public class RandomizedSelectTest {

    @Test
    public void testRandomizedSelectUniqueLowI() {
        // Arrange
        int[] a = new int[]{60, 42, 23, 91, 14, 87, 55, 39, 72};
        int expected = 14;
        // Act
        int actual = RandomizedSelect.find(a, 0, a.length - 1, 1);
        // Assert
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testRandomizedSelectUniqueMidI() {
        // Arrange
        int[] a = new int[]{60, 42, 23, 91, 14, 87, 55, 39, 72};
        int expected = 55;
        // Act
        int actual = RandomizedSelect.find(a, 0, a.length - 1, 5);
        // Assert
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testRandomizedSelectUniqueHighI() {
        // Arrange
        int[] a = new int[]{60, 42, 23, 91, 14, 87, 55, 39, 72};
        int expected = 91;
        // Act
        int actual = RandomizedSelect.find(a, 0, a.length - 1, 9);
        // Assert
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testRandomizedSelectRepeating() {
        // Arrange
        int[] a = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1};
        int expected = 1;
        // Act
        int actual = RandomizedSelect.find(a, 0, a.length - 1, 3);
        // Assert
        Assert.assertEquals(expected, actual);
    }

}
