import java.util.Random;

public class RandomQuickSort {

    static int randomPartition(int[] a, int p, int r) {
        int i = new Random().ints(p, r+1).limit(1).findFirst().orElse(-1);
        Helpers.swap(a, r, i);
        return QuickSort.partition(a, p, r);
    }

    static void sort(int[] a, int p, int r) {
        if (p < r) {
            int q = randomPartition(a, p, r);
            sort(a, p, q - 1);
            sort(a, q + 1, r);
        }
    }

}
