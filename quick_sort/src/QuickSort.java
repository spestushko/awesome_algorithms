public class QuickSort {

    public static int partition(int[] a, int p, int r) {
        int x = a[r];
        int i = p - 1;
        for (int j = p; j <= r - 1; ++j) {
            if (a[j] <= x) {
                i += 1;
                Helpers.swap(a, i, j);
            }
        }
        Helpers.swap(a, i + 1, r);
        return i + 1;
    }


    static void sort(int[] a, int p, int r) {
        if (p < r) {
            int q = partition(a, p, r);
            sort(a, p, q - 1);
            sort(a, q + 1, r);
        }
    }
}
