import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class QuickSortTest {

    @Test
    public void testQuickSort() {

        // Arrange
        int[] a = new int[]{2, 8, 7, 1, 3, 5, 6, 4};
        int[] e = new int[]{1, 2, 3, 4, 5, 6, 7, 8};

        // Act
        QuickSort.sort(a, 0, a.length - 1);

        // Assert
        assertArrayEquals(e, a);
    }

    @Test
    public void testPartition() {

        // Arrange
        int[] a = new int[]{2, 8, 7, 1, 3, 5, 6, 4};
        int e = 3;

        // Act
        int r = QuickSort.partition(a, 0, a.length - 1);

        // Assert
        assertEquals(e, r);
    }

}
