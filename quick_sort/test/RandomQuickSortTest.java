import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class RandomQuickSortTest {

    @Test
    public void testQuickSort() {
        // Arrange
        int[] a = new int[]{2, 8, 7, 1, 3, 5, 6, 4};
        int[] e = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        // Act
        RandomQuickSort.sort(a, 0, a.length - 1);
        // Assert
        assertArrayEquals(e, a);
    }

    @Test
    public void testQuickSortAllSame() {
        // Arrange
        int[] a = new int[]{2, 2, 2, 2, 2, 2, 2, 2};
        int[] e = new int[]{2, 2, 2, 2, 2, 2, 2, 2};
        // Act
        RandomQuickSort.sort(a, 0, a.length - 1);
        // Assert
        assertArrayEquals(e, a);
    }

    @Test
    public void testQuickSortTwoBiggest() {
        // Arrange
        int[] a = new int[]{2, 8, 7, 1, 3, 5, 6, 1};
        int[] e = new int[]{1, 1, 2, 3, 5, 6, 7, 8};
        // Act
        RandomQuickSort.sort(a, 0, a.length - 1);
        // Assert
        assertArrayEquals(e, a);
    }

    @Test
    public void testQuickSortDuplicates() {
        // Arrange
        int[] a = new int[]{7, 8, 7, 8, 6, 5, 6, 5};
        int[] e = new int[]{5, 5, 6, 6, 7, 7, 8, 8};
        // Act
        RandomQuickSort.sort(a, 0, a.length - 1);
        // Assert
        assertArrayEquals(e, a);
    }
}
