class RadixSort {

    static int findMax(int[] a, int offset) {
        int max = Helpers.getIntAt(a[0], offset);
        for (int i = 1; i < a.length; ++i) {
            int tmp = Helpers.getIntAt(a[i], offset);
            max = Math.max(max, tmp);
        }
        return max;
    }

    static void sort(int[] a, int d) {
        for (int i = d-1; i >= 0; i--) {
            int max = findMax(a, i);
            CountingSort.indexSort(a, a.length, max, i);
        }
    }

}
