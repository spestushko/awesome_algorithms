class CountingSort {

    static void sort(int[] a, int n, int k) {
        int[] b = new int[n];
        int[] c = new int[k+1];  // Array is filled with 0 by default
        for (int j = 0; j < n; ++j) c[a[j]] = c[a[j]] + 1;  // c[i] will contain # of elements equal to i
        for (int i = 1; i <= k; ++i) c[i] = c[i] + c[i - 1];  // c[i] will contain # of elements less than or equal to i
        for (int j = n - 1; j >= 0; j--) {
            b[c[a[j]] - 1] = a[j];
            c[a[j]] = c[a[j]] - 1;
        }
        System.arraycopy(b, 0, a, 0, a.length);
    }

    static void indexSort(int[] a, int n, int k, int offset) {
        int[] b = new int[n];
        int[] c = new int[k+1];  // Array is filled with 0 by default
        for (int j = 0; j < n; ++j) c[Helpers.getIntAt(a[j], offset)] = c[Helpers.getIntAt(a[j], offset)] + 1;
        for (int i = 1; i <= k; ++i) c[i] = c[i] + c[i - 1];
        for (int j = n - 1; j >= 0; j--) {
            b[c[Helpers.getIntAt(a[j], offset)] - 1] = a[j];
            c[Helpers.getIntAt(a[j], offset)] = c[Helpers.getIntAt(a[j], offset)] - 1;
        }
        System.arraycopy(b, 0, a, 0, a.length);
    }

}
