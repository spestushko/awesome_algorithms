import java.util.*;

public class BucketSort {

    static void sort(double[] a) {
        // Todo: Make algorithm work for arbitrary array length
        int n = a.length; // Works if array size is 10, 100, 1000...
        Map<Integer, List<Double>> b = new HashMap<>();

        // Iterate through array and fill up buckets
        for (double v : a) {
            int bi = (int) (n * v);
            if (b.get(bi) == null) {
                List<Double> bucket = new ArrayList<>();
                b.put(bi, bucket);
            }
            b.get(bi).add(v);
        }

        // Concatenate buckets after sorting each one
        List<Double> allBuckets = new ArrayList<>();
        for (Map.Entry<Integer, List<Double>> entry : b.entrySet()) {
            List<Double> bucket = entry.getValue();
            Collections.sort(bucket);
            allBuckets.addAll(bucket);
        }
        double[] res = allBuckets.stream().mapToDouble(Double::doubleValue).toArray();

        // Override original array
        System.arraycopy(res, 0, a, 0, a.length);

    }

}
