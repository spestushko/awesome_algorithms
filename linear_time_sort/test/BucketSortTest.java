import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class BucketSortTest {

    @Test
    public void bucketSortTest() {
        // Arrange
        double[] a = new double[]{.78, .17, .39, .26, .72, .94, .21, .12, .23, .68};
        double[] e = new double[]{.12, .17, .21, .23, .26, .39, .68, .72, .78, .94};
        // Act
        BucketSort.sort(a);
        // Assert
        assertTrue(Arrays.equals(a, e));
    }

    @Test
    public void bucketSortTestSameElements() {
        // Arrange
        double[] a = new double[]{.12, .12, .12, .12, .12, .12, .12, .12, .12, .12};
        double[] e = new double[]{.12, .12, .12, .12, .12, .12, .12, .12, .12, .12};
        // Act
        BucketSort.sort(a);
        // Assert
        assertTrue(Arrays.equals(a, e));
    }

    @Test
    public void bucketSortTestUniqueElements() {
        // Arrange
        double[] a = new double[]{.33, .92, .21, .56, .12, 1.01, .78, .45, .69, .89};
        double[] e = new double[]{.12, .21, .33, .45, .56, .69, .78, .89, .92, 1.01};
        // Act
        BucketSort.sort(a);
        // Assert
        assertTrue(Arrays.equals(a, e));
    }
}
