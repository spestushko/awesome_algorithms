import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class RadixSortTest {

    @Test
    public void testRadixSort() {
        // Arrange
        int[] a = new int[]{329, 457, 657, 839, 436, 720, 355};
        int[] e = new int[]{329, 355, 436, 457, 657, 720, 839};
        int d = 3;
        // Act
        RadixSort.sort(a, d);
        // Assert
        System.out.println(Arrays.toString(a));
        assertArrayEquals(e, a);
    }

    @Test
    public void testRadixSortAllEqual() {
        // Arrange
        int[] a = new int[]{444, 444, 444, 444, 444, 444, 444};
        int[] e = new int[]{444, 444, 444, 444, 444, 444, 444};
        int d = 3;
        // Act
        RadixSort.sort(a, d);
        // Assert
        System.out.println(Arrays.toString(a));
        assertArrayEquals(e, a);
    }

}
