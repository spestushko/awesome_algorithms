import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertArrayEquals;


public class CountingSortTest {

    @Test
    public void testCountingSort() {
        // Arrange
        int[] a = new int[]{2, 5, 3, 0, 2, 3, 0, 3};
        int[] e = new int[]{0, 0, 2, 2, 3, 3, 3, 5};
        // Act
        CountingSort.sort(a, a.length, Arrays.stream(a).max().getAsInt());
        // Assert
        assertArrayEquals(e, a);
    }

    @Test
    public void testCountingSortOnUniqueElements() {
        // Arrange
        int[] a = new int[]{8, 7, 6, 5, 4, 3, 2, 1};
        int[] e = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        // Act
        int[] b = new int[a.length];
        CountingSort.sort(a, a.length, Arrays.stream(a).max().getAsInt());
        // Assert
        assertArrayEquals(e, a);
    }

    @Test
    public void testCountingSortOnEqualElements() {
        // Arrange
        int[] a = new int[]{4, 4, 4, 4, 4, 4, 4, 4};
        int[] e = new int[]{4, 4, 4, 4, 4, 4, 4, 4};
        // Act
        int[] b = new int[a.length];
        CountingSort.sort(a, a.length, Arrays.stream(a).max().getAsInt());
        // Assert
        assertArrayEquals(e, a);
    }

}
