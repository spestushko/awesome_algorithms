import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HelpersTest {

    @Test
    public void testSwap() {
        int[] a = new int[]{1, 2, 3, 4, 5};
        Helpers.swap(a, 0, 4);
        assertEquals(a[0], 5);
        assertEquals(a[a.length - 1], 1);
    }

    @Test
    public void testMaxArraySizeCompareEven() {

        // Arrange
        int[][] a = new int[][]{
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
        };
        int[][] b = new int[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3}
        };

        // Act
        int res = Helpers.maxMatrixDimension(a, b);

        // Assert
        assertEquals(5, res);
    }

    @Test
    public void testMaxArraySizeCompareUnevenWidth() {

        // Arrange
        int[][] a = new int[][]{
                {1, 2, 3, 4, 5},
                {1, 2, 3},
                {1, 2},
        };
        int[][] b = new int[][]{
                {1, 2, 3},
                {1, 2, 3, 4},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5, 6},
        };

        // Act
        int res = Helpers.maxMatrixDimension(a, b);

        // Assert
        assertEquals(6, res);
    }

    @Test
    public void testMaxArraySizeCompareUnevenHeight() {

        // Arrange
        int[][] a = new int[][]{
                {1, 2, 3, 4, 5},
                {1, 2, 3},
                {1, 2},
        };
        int[][] b = new int[][]{
                {1, 2, 3},
                {1, 2, 3, 4},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
        };

        // Act
        int res = Helpers.maxMatrixDimension(a, b);

        // Assert
        assertEquals(8, res);
    }


    @Test
    public void testFillMatrixWithZeroes() {
        // Arrange
        int[][] a = new int[3][3];

        // Act
        Helpers.fillZeroes(a);

        // Assert
        assertTrue(Arrays.deepEquals(a, new int[][] {
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0}
        }));
    }

    @Test
    public void testGetIntAt() {
        assertEquals(9, Helpers.getIntAt(329, 2));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testGetIntAtNotExist() {
        assertEquals(9, Helpers.getIntAt(329, 5));
    }
}
