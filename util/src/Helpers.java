import java.util.Arrays;

public class Helpers {


    /**
     * Swaps elements of the passed in array
     * @param src - source array
     * @param i - swap index
     * @param j - swap index
     */
    public static void swap(int[] src, int i, int j) {
        int tmp = src[i];
        src[i] = src[j];
        src[j] = tmp;
    }


    /**
     * Compares array sizes and returns the size of the biggest array
     * @param a - source array
     * @param b - source array
     * @return size of the bigger array
     */
    public static int maxMatrixDimension(int[][] a, int[][] b) {
        int ysize = Math.max(a.length, b.length);

        int axsize = Math.max(a[0].length, b[0].length);
        for (int[] ints : a) axsize = Math.max(axsize, ints.length);

        int bxsize = Math.max(a[0].length, b[0].length);
        for (int[] ints : b) bxsize = Math.max(bxsize, ints.length);

        return Math.max(ysize, Math.max(axsize, bxsize));
    }


    /**
     * This method fills a 2d matrix with zeroes
     * @param a - source 2d matrix
     */
    public static void fillZeroes(int[][] a) {
        for (int[] row: a)
            Arrays.fill(row, 0);
    }


    /**
     * This method prints out a 2d matrix in a readable format
     * @param a - source 2d matrix
     */
    public static void prettyPrint2DMatrix(int[][] a) {
        for (int[] row: a)
            System.out.println(Arrays.toString(row)
                    .replace("[", "")
                    .replace("]", ""));
    }

    /**
     * This method extracts an integer value from a specified position in the number
     *
     * Examples:
     * getIntAt(329, 2) - returns 9
     *
     * @param v - original number
     * @param i - index
     * @return int
     */
    static int getIntAt(int v, int i) {
        if (i < 0 || i > Integer.toString(v).length())
            throw new IllegalArgumentException("Illegal index");
        return Character.getNumericValue(Integer.toString(v).charAt(i));
    }
}
