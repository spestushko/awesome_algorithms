import java.util.Arrays;

public class Main {

    static class InsertionSort {

        static int[] sortInc(int[] data) {
            for (int i = 1; i < data.length; ++i) {
                int key = data[i];
                int j = i - 1;
                while (j >= 0 && data[j] > key) {
                    data[j + 1] = data[j];
                    j -= 1;
                }
                data[j + 1] = key;
            }

            return data;
        }

        static int[] sortDec(int[] data) {
            for (int i = 1; i < data.length; ++i) {
                int key = data[i];
                int j = i - 1;
                while (j >= 0 && data[j] < key) {
                    data[j + 1] = data[j];
                    j -= 1;
                }
                data[j + 1] = key;
            }

            return data;
        }
    }

    public static void main(String[] args) {
        int[] testData = { 5, 2, 4, 6, 1, 3};
        System.out.println("\nINSERTION SORT\n");
        System.out.println("Before sort: ".concat(Arrays.toString(testData)));
        System.out.println("↑After sort: ".concat(Arrays.toString(InsertionSort.sortInc(testData))));
        System.out.println("↓After sort: ".concat(Arrays.toString(InsertionSort.sortDec(testData))));
    }
}
