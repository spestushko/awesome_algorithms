import static java.lang.System.*;

public class MaxSubArray {

    static class DivNCnq {

        private static final int SUM = 2;

        static int[] findCrossingSubArray(int[] a, int low, int mid, int high) {
            int sum = 0;
            int leftSum = 0;
            int maxLeft = -1;

            for (int i = mid; i >= low; --i) {
                sum += a[i];
                if (sum > leftSum) {
                    leftSum = sum;
                    maxLeft = i;
                }
            }

            sum = 0;
            int rightSum = 0;
            int maxRight = -1;

            for (int i = mid + 1; i < high; ++i) {
                sum += a[i];
                if (sum > rightSum) {
                    rightSum = sum;
                    maxRight = i;
                }
            }

            return new int[]{maxLeft, maxRight, leftSum + rightSum};
        }

        static int[] findMaxSubArray(int[] a, int low, int high) {
            if (low == high) {
                return new int[]{low, high, a[low]};
            } else {
                int mid = (low + high) / 2;

                int[] leftSub = findMaxSubArray(a, low, mid);
                int[] rightSub = findMaxSubArray(a, mid+1, high);
                int[] crossSub = findCrossingSubArray(a, low, mid, high);

                if (leftSub[SUM] >= rightSub[SUM] && leftSub[SUM] >= crossSub[SUM])
                    return leftSub;
                else if(rightSub[SUM] >= leftSub[SUM] && rightSub[SUM] >= crossSub[SUM])
                    return rightSub;
                else return crossSub;
            }
        }
    }

    static class Quadratic {
        static int[] findMaxSubArray(int[] a) {
            int bestSum = Integer.MIN_VALUE;
            int sumSoFar;
            int leftSumIdx = 0;
            int rightSumIdx = 0;

            for (int i = 0; i < a.length - 1; ++i) {
                sumSoFar = a[i];
                for (int j = i+1; j < a.length; ++j) {
                    sumSoFar += a[j];

                    if (sumSoFar > bestSum) {
                        bestSum = sumSoFar;
                        leftSumIdx = i;
                        rightSumIdx = j;
                    }

                }
            }

            return new int[]{leftSumIdx, rightSumIdx, bestSum};
        }
    }

    static class Linear {
        static int[] findMaxSubArray(int[] a) {
            int leftSumIdx = -1;
            int rightSumIdx = -1;

            int bestSum = 0;
            int sumSoFar = 0;

            int l_r = 0;

            for (int i = 0; i < a.length; ++i) {
                sumSoFar += a[i];

                int tmpBestSum = bestSum;
                bestSum = Math.max(bestSum, sumSoFar);
                if (tmpBestSum != bestSum) {
                    leftSumIdx = l_r;
                    rightSumIdx = i;
                }


                int tmpSumSoFar = sumSoFar;
                sumSoFar = Math.max(sumSoFar, 0);
                if (tmpSumSoFar != sumSoFar) {
                    l_r = i+1;
                }
            }

            return new int[]{leftSumIdx, rightSumIdx, bestSum};
        }
    }

    public static void main(String[] args) {
        out.println("=============");
        out.println("Max sub array");
    }

}
