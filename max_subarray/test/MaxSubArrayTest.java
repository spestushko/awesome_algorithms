import org.junit.Test;


import static org.junit.Assert.assertArrayEquals;

public class MaxSubArrayTest {

    @Test
    public void findSubArrayDivNCnqCross() {

        int[] src = new int[]{13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
        int[] expected = new int[]{7, 10, 43};

        int[] maxSub = MaxSubArray.DivNCnq.findMaxSubArray(src, 0, src.length - 1);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubArrayDivNCnqLeft() {

        int[] src = new int[]{13, -3, 25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
        int[] expected = new int[]{0, 10, 56};

        int[] maxSub = MaxSubArray.DivNCnq.findMaxSubArray(src, 0, src.length - 1);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubArrayDivNCnqRight() {

        int[] src = new int[]{13, -3, -25, 20, -3, -16, -23, -18, 20, -7, -12, 8, 22, 15, 4, -7};
        int[] expected = new int[]{8, 14, 50};

        int[] maxSub = MaxSubArray.DivNCnq.findMaxSubArray(src, 0, src.length - 1);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findMaxCrossingSubArray() {

        int[] src = new int[]{13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
        int[] expected = new int[]{7, 10, 43};

        int low = 0;
        int high = src.length - 1;
        int mid = (low + high) / 2;
        int[] maxSub = MaxSubArray.DivNCnq.findCrossingSubArray(src, low, mid, high);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubElementQuadratic() {
        int[] src = new int[]{13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
        int[] expected = new int[]{7, 10, 43};

        int[] maxSub = MaxSubArray.Quadratic.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubElementQuadraticLeft() {
        int[] src = new int[]{5, 10, 15, -10, -20, 10, 15};
        int[] expected = new int[]{0, 2, 30};

        int[] maxSub = MaxSubArray.Quadratic.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubElementQuadraticRight() {
        int[] src = new int[]{5, 10, -15, -10, -20, 10, 15, 10};
        int[] expected = new int[]{5, 7, 35};

        int[] maxSub = MaxSubArray.Quadratic.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubElementQuadraticMid() {
        int[] src = new int[]{-5, -10, 15, 10, 20, -10, -15, -10};
        int[] expected = new int[]{2, 4, 45};

        int[] maxSub = MaxSubArray.Quadratic.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

   @Test
    public void findSubElementQaudraticLeft2() {

        int[] src = new int[]{13, -3, 25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
        int[] expected = new int[]{0, 10, 56};

        int[] maxSub = MaxSubArray.Quadratic.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubElementQuadraticRight2() {

        int[] src = new int[]{13, -3, -25, 20, -3, -16, -23, -18, 20, -7, -12, 8, 22, 15, 4, -7};
        int[] expected = new int[]{8, 14, 50};

        int[] maxSub = MaxSubArray.Quadratic.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubArrayLinearLeft() {
        int[] src = new int[]{5, 10, 15, -20, -30, 10, 5};
        int[] expected = new int[]{0, 2, 30};

        int[] maxSub = MaxSubArray.Linear.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubArrayLinearCrossing() {
        int[] src = new int[]{13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
        int[] expected = new int[]{7, 10, 43};

        int[] maxSub = MaxSubArray.Linear.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubArrayLinearLeft2() {

        int[] src = new int[]{13, -3, 25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
        int[] expected = new int[]{0, 10, 56};

        int[] maxSub = MaxSubArray.Linear.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

    @Test
    public void findSubArrayLinearRight2() {

        int[] src = new int[]{13, -3, -25, 20, -3, -16, -23, -18, 20, -7, -12, 8, 22, 15, 4, -7};
        int[] expected = new int[]{8, 14, 50};

        int[] maxSub = MaxSubArray.Linear.findMaxSubArray(src);

        assertArrayEquals(expected, maxSub);
    }

}
