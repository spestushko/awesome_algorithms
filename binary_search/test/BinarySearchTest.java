import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class BinarySearchTest {

    @Test
    public void testEveryElementOdd() {

        int[] src = new int[]{12, 18, 29, 45, 64, 76, 82, 99, 101, 132, 135};
        int[] expected = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] res = new int[src.length];

        for (int i = 0; i < src.length; ++i)
            res[i] = BinarySearch.search(src, 0, src.length - 1, src[i]);

        assertArrayEquals(expected, res);
    }

    @Test
    public void testEveryElementEven() {

        int[] src = new int[]{12, 18, 29, 45, 64, 76, 82, 99, 101, 132};
        int[] expected = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] res = new int[src.length];

        for (int i = 0; i < src.length; ++i)
            res[i] = BinarySearch.search(src, 0, src.length - 1, src[i]);

        assertArrayEquals(expected, res);
    }

    @Test
    public void testNonExisting() {

        int[] src = new int[]{12, 18, 29, 45, 64, 76, 82, 99, 101, 132, 135};
        int find = 100;
        int expected = -1;
        int res;

        res = BinarySearch.search(src, 0, src.length - 1, find);

        assertEquals(expected, res);
    }

}
