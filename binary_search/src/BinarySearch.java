
public class BinarySearch {

    static int search(int[] a, int l, int r, int v) {

        if (l == r)
            return a[l] == v ? l : -1;

        int mod = (l+r) / 2;
        if (a[mod] == v)
            return mod;

        if (a[mod] > v)
            return search(a, l, mod, v);
        else
            return search(a, mod + 1, r, v);

    }

    public static void main(String[] args) {
        int[] a = new int[]{12, 18, 29, 45, 64, 76, 82, 99, 101, 132, 135};
        int find = 132;
        System.out.println("\nBINARY SEARCH\n");
        System.out.println("Found element: ".concat(Integer.toString(find))
                                            .concat(" at index "
                                            .concat(Integer.toString(BinarySearch.search(a, 0, a.length - 1, find)))));
    }

}
