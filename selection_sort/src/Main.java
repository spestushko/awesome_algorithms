import java.util.Arrays;

public class Main {

    static class Selection {

        static int[] sortInc(int[] a) {
            int n = a.length;
            for (int i = 0; i < n - 1; ++i) {
                int min = i;
                for (int j = i + 1; j < n; ++j)
                    if (a[j] < a[min])
                        min = j;
                int tmp = a[min];
                a[min] = a[i];
                a[i] = tmp;
            }
            return a;
        }

        static int[] sortDec(int[] a) {
            int n = a.length;
            for (int i = 0; i < n - 1; ++i) {
                int max = i;
                for (int j = i + 1; j < n; ++j)
                    if (a[j] > a[max])
                        max = j;
                int tmp = a[max];
                a[max] = a[i];
                a[i] = tmp;
            }
            return a;
        }
    }

    public static void main(String[] args) {
        int[] testData = new int[]{ 5, 2, 4, 6, 1, 3 };
        System.out.println("\nSELECTION SORT\n");
        System.out.println("Before sort: ".concat(Arrays.toString(testData)));
        System.out.println("↑After sort: ".concat(Arrays.toString(Selection.sortInc(testData))));
        System.out.println("↓After sort: ".concat(Arrays.toString(Selection.sortDec(testData))));
    }

}
