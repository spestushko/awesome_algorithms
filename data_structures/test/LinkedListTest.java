import org.junit.Assert;
import org.junit.Test;

public class LinkedListTest {

    @Test
    public void testInitLinkedList() {
        // Arrange
        LinkedList<String> linkedList = new LinkedList<>("Alice");
        // Assert
        Assert.assertNotNull(linkedList);
    }

    @Test
    public void testInsert() {
        // Arrange
        LinkedList<String> linkedList = new LinkedList<>("Alice");
        // Act
        linkedList.insert("Bob");
        linkedList.print();
        // Assert
        Assert.assertNotNull(linkedList);
    }

    @Test
    public void testFindNodeExists() {
        // Arrange
        LinkedList<String> linkedList = new LinkedList<>("Alice");
        String expectedName = "Kevin";
        // Act
        linkedList.insert("Bob");
        linkedList.insert("Mark");
        linkedList.insert(expectedName);
        LinkedList.Node searchResult = linkedList.find(expectedName);
        // Assert
        Assert.assertNotNull(searchResult);
        Assert.assertEquals(expectedName, String.valueOf(searchResult.getKey()));
    }

    @Test
    public void testFindNodeMissing() {
        // Arrange
        LinkedList<String> linkedList = new LinkedList<>("Alice");
        String expectedName = "Spencer";
        // Act
        linkedList.insert("Bob");
        linkedList.insert("Mark");
        linkedList.insert("Kevin");
        linkedList.print();
        LinkedList.Node searchResult = linkedList.find(expectedName);
        // Assert
        Assert.assertNull(searchResult);
    }

    @Test
    public void testRemoveNodeMedian() {
        // Arrange
        String[] org = new String[] {"Alice", "Bob", "Mark", "Kevin", "Spencer"};
        String[] expected = new String[] {"Spencer", "Kevin", "Bob", "Alice"};

        LinkedList<String> linkedList = new LinkedList<>(org[0]);

        for (int i = 1; i < org.length; ++i)
            linkedList.insert(org[i]);

        linkedList.print();

        // Act
        LinkedList.Node toRemove = linkedList.find("Mark");
        linkedList.delete(toRemove);
        linkedList.print();

        // Assert
        Assert.assertArrayEquals(expected, linkedList.toArray(new String[expected.length]));
    }

    @Test
    public void testRemoveNodeFirst() {
        // Arrange
        String[] org = new String[] {"Alice", "Bob", "Mark", "Kevin", "Spencer"};
        String[] expected = new String[] {"Kevin", "Mark", "Bob", "Alice"};

        LinkedList<String> linkedList = new LinkedList<>(org[0]);

        for (int i = 1; i < org.length; ++i)
            linkedList.insert(org[i]);

        linkedList.print();

        // Act
        LinkedList.Node toRemove = linkedList.find("Spencer");
        linkedList.delete(toRemove);
        linkedList.print();

        // Assert
        Assert.assertArrayEquals(expected, linkedList.toArray(new String[expected.length]));
    }

    @Test
    public void testRemoveNodeLast() {
        // Arrange
        String[] org = new String[] {"Alice", "Bob", "Mark", "Kevin", "Spencer"};
        String[] expected = new String[] {"Spencer", "Kevin", "Mark", "Bob"};

        LinkedList<String> linkedList = new LinkedList<>(org[0]);

        for (int i = 1; i < org.length; ++i)
            linkedList.insert(org[i]);

        linkedList.print();

        // Act
        LinkedList.Node toRemove = linkedList.find("Alice");
        linkedList.delete(toRemove);
        linkedList.print();

        // Assert
        Assert.assertArrayEquals(expected, linkedList.toArray(new String[expected.length]));
    }
}
