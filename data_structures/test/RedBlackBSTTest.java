import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class RedBlackBSTTest {

    @Test
    public void testCreateBST() {
        // Arrange
        RedBlackBST redBlackBST = new RedBlackBST();
        // Assert
        Assert.assertNotNull(redBlackBST);
    }

    @Test
    public void testInsertHead() {
        // Arrange
        RedBlackBST redBlackBST = new RedBlackBST();
        // Act
        redBlackBST.insert(new RedBlackBST.RBTreeNode(42));
        // Assert
        Assert.assertEquals(42, redBlackBST.head.key);
        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.color);
    }

    @Test
    public void testInsertTreeNodes() {
        // Arrange
        RedBlackBST redBlackBST = new RedBlackBST();
        // Act
        for (int i: new int[] {11, 2, 14, 1, 15, 7, 5, 8, 4}) redBlackBST.insert(new RedBlackBST.RBTreeNode(i));

        // Assert
        // ROOT.key
        Assert.assertEquals(7, redBlackBST.head.key);
        // ROOT->LEFT...key
        Assert.assertEquals(2, redBlackBST.head.left.key);
        Assert.assertEquals(1, redBlackBST.head.left.left.key);
        Assert.assertEquals(5, redBlackBST.head.left.right.key);
        Assert.assertEquals(4, redBlackBST.head.left.right.left.key);
        // ROOT->RIGHT..key
        Assert.assertEquals(11, redBlackBST.head.right.key);
        Assert.assertEquals(8,  redBlackBST.head.right.left.key);
        Assert.assertEquals(14, redBlackBST.head.right.right.key);
        Assert.assertEquals(15, redBlackBST.head.right.right.right.key);

        // ROOT.COLOR
        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.color);

        // ROOT->LEFT...color
        Assert.assertEquals(RedBlackBST.Color.RED, redBlackBST.head.left.color);
        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.left.left.color);
        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.left.right.color);
        Assert.assertEquals(RedBlackBST.Color.RED, redBlackBST.head.left.right.left.color);

        // ROOT->RIGHT...color
        Assert.assertEquals(RedBlackBST.Color.RED, redBlackBST.head.right.color);
        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.right.right.color);
        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.right.left.color);
        Assert.assertEquals(RedBlackBST.Color.RED, redBlackBST.head.right.right.right.color);
    }

    @Test
    public void testDeletedNodeIsBlackChildIsRed() {
        // Arrange
        RedBlackBST redBlackBST = new RedBlackBST();
        for (int i: new int[]{30, 20, 40, 10}) redBlackBST.insert(new RedBlackBST.RBTreeNode(i));
        RedBlackBST.RBTreeNode nodeToDelete = redBlackBST.head.left;

        // Act
        redBlackBST.delete(nodeToDelete);

        // Assert
        // Colors
        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.color);
        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.left.color);
        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.right.color);

        // Keys
        Assert.assertEquals(30, redBlackBST.head.key);
        Assert.assertEquals(10, redBlackBST.head.left.key);
        Assert.assertEquals(40, redBlackBST.head.right.key);
    }

    // Todo: Add more tests covering all deletion examples
//    @Test
//    public void testDeletedNodeIsBlackChildIsBlack() {
//        // Arrange
//        RedBlackBST redBlackBST = new RedBlackBST();
//        for (int i: new int[]{30, 20, 40, 50}) redBlackBST.insert(new RedBlackBST.RBTreeNode(i));
//        RedBlackBST.RBTreeNode nodeToDelete = redBlackBST.head.left;
//
//        // Act
//        redBlackBST.delete(nodeToDelete);
//
//        // Assert
//        // Colors
//        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.color);
//        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.left.color);
//        Assert.assertEquals(RedBlackBST.Color.BLACK, redBlackBST.head.right.color);
////        Assert.assertEquals(RedBlackBST.Color.RED, redBlackBST.head.right.right.color);
//
//        // Keys
//        Assert.assertEquals(30, redBlackBST.head.key);
//        Assert.assertEquals(40, redBlackBST.head.right.key);
//        Assert.assertEquals(50, redBlackBST.head.right.right.key);
//    }
}
