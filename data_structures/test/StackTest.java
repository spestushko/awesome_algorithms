import exceptions.StackException;
import org.junit.Test;
import org.junit.Assert;

public class StackTest {

    private final String[] stackArgs = new String[] {"Hello", "World", "From", "Java"};

    @Test
    public void testStringEmptyStack() {
        // Arrange
        Stack<String> stack = new Stack<>(3);
        // Assert
        Assert.assertTrue(stack.isEmpty());
    }

    @Test
    public void testStringNonEmptyStack() {
        // Arrange
        Stack<String> stack = new Stack<>(3);
        // Act
        stack.push("Hello");
        // Assert
        Assert.assertFalse(stack.isEmpty());
    }

    @Test
    public void testStringPushElementsNoResize() throws StackException {
        // Arrange
        Stack<String> stack = new Stack<>(5);
        // Act
        for (String s: this.stackArgs)
            stack.push(s);
        // Assert
        for (int i = this.stackArgs.length - 1; i >= 0; --i)
            Assert.assertEquals(stack.pop(), this.stackArgs[i]);
    }

    @Test
    public void testStringPushElementsResize() throws StackException {
        // Arrange
        Stack<String> stack = new Stack<>(3);
        // Act
        for (String s: this.stackArgs)
            stack.push(s);
        // Assert
        for (int i = this.stackArgs.length - 1; i >= 0; --i)
            Assert.assertEquals(stack.pop(), this.stackArgs[i]);
    }

    @Test (expected = StackException.class)
    public void testStringPopStackUnderflow() throws StackException {
        // Arrange
        Stack<String> stack = new Stack<>(2);
        // Act
        stack.push("Hello");
        stack.pop();
        stack.pop();
    }

}
