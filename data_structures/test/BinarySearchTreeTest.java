import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class BinarySearchTreeTest {

    @Test
    public void testCreateBST() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        // Act
        insertTestValues(bst);
        // Assert
        Assert.assertNotNull(bst.head);
    }

    @Test
    public void testFindAtRoot() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        // Act
        insertTestValues(bst);
        // Assert
        Assert.assertEquals(5, bst.findTreeNode(bst.head, 5).key);
    }

    @Test
    public void testFindAtLeaf() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        // Act
        insertTestValues(bst);
        // Assert
        Assert.assertEquals(2, bst.findTreeNode(bst.head, 2).key);
    }

    @Test
    public void testFindMidTree() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        // Act
        insertTestValues(bst);
        // Asssert
        Assert.assertEquals(4, bst.findTreeNode(bst.head, 4).key);
    }

    @Test
    public void testNotFound() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        // Act
        insertTestValues(bst);
        // Assert
        Assert.assertEquals(null, bst.findTreeNode(bst.head, 42));
    }

    @Test
    public void testDistanceToRoot() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode start = bst.head;
        BinarySearchTree.TreeNode end = bst.findTreeNode(start, 5);
        // Assert
        Assert.assertEquals(0, bst.distanceToFromNode(start, end, 0));
    }

    @Test
    public void testDistanceToLeaf() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode start = bst.head;
        BinarySearchTree.TreeNode end = bst.findTreeNode(start, 2);
        // Assert
        Assert.assertEquals(3, bst.distanceToFromNode(start, end, 0));
    }

    @Test
    public void testDistanceToMidTreeLeaf() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode start = bst.head;
        BinarySearchTree.TreeNode end = bst.findTreeNode(start, 6);
        // Assert
        Assert.assertEquals(2, bst.distanceToFromNode(start, end, 0));
    }

    @Test
    public void testDistanceBetweenLeafs() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode start = bst.findTreeNode(bst.head, 2);
        BinarySearchTree.TreeNode end = bst.findTreeNode(bst.head, 4);
        // Assert
        Assert.assertEquals(3, bst.distanceToFromNode(start, end, 0));
    }

    @Test
    public void testDistanceBetweenLeafsRightSideOfRoot() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode start = bst.findTreeNode(bst.head, 6);
        BinarySearchTree.TreeNode end = bst.findTreeNode(bst.head, 4);
        // Assert
        Assert.assertEquals(4, bst.distanceToFromNode(start, end, 0));
    }

    @Test
    public void testDistanceBetweenLeafsFurthestLeafs() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode start = bst.findTreeNode(bst.head, 6);
        BinarySearchTree.TreeNode end = bst.findTreeNode(bst.head, 2);
        // Assert
        Assert.assertEquals(5, bst.distanceToFromNode(start, end, 0));
    }

    @Test
    public void testDistanceBetweenChildAndNode() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode start = bst.findTreeNode(bst.head, 5);
        BinarySearchTree.TreeNode end = bst.findTreeNode(bst.head, 1);
        // Assert
        Assert.assertEquals(2, bst.distanceToFromNode(start, end, 0));
    }

    @Test
    public void testDistanceFromRootBetweenLeafs() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        int start = 2;
        int end = 4;
        // Assert
        Assert.assertEquals(3, bst.distanceToFromRoot(bst.head, start, end));
    }

    @Test
    public void testDistanceFromRootBetweenLeafsRightSideOfRoot() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        int start = 4;
        int end = 6;
        // Assert
        Assert.assertEquals(4, bst.distanceToFromRoot(bst.head, start, end));
    }

    @Test
    public void testDistanceFromRootBetweenLeafsFurthestLeafs() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        int start = 2;
        int end = 6;
        // Assert
        Assert.assertEquals(5, bst.distanceToFromRoot(bst.head, start, end));
    }

    @Test
    public void testDistanceFromRootBetweenChildAndNode() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        int start = 1;
        int end = 5;
        // Assert
        Assert.assertEquals(2, bst.distanceToFromRoot(bst.head, start, end));
    }

    @Test
    public void testMinimum() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode min = bst.minimum(bst.head);
        // Assert
	    Assert.assertEquals(1, min.key);
    }

    @Test
    public void testMinimumWithNullRoot() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode min = bst.minimum(null);
        // Assert
	    Assert.assertNull(min);
    }

    @Test
    public void testMaximum() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode min = bst.maximum(bst.head);
        // Assert
	    Assert.assertEquals(7, min.key);
    }

    @Test
    public void testMaximumWithNullRoot() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode min = bst.maximum(null);
        // Assert
	    Assert.assertNull(min);
    }

    @Test
    public void testSuccessorRightNode() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode successor = bst.successor(bst.head);
        // Assert
        Assert.assertEquals(6, successor.key);
    }

    @Test
    public void testSuccessorLeftNode() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        BinarySearchTree.TreeNode startNode = bst.findTreeNode(bst.head, 4);
        // Act
        BinarySearchTree.TreeNode successor = bst.successor(startNode);
        // Assert
        Assert.assertEquals(5, successor.key);
    }

    @Test
    public void testPredecessorRightNode() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        BinarySearchTree.TreeNode startNode = bst.findTreeNode(bst.head, 6);
        // Act
        BinarySearchTree.TreeNode successor = bst.predecessor(startNode);
        // Assert
        Assert.assertEquals(5, successor.key);
    }

    @Test
    public void testPredecessorLeftNode() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        // Act
        BinarySearchTree.TreeNode successor = bst.predecessor(bst.head);
        // Assert
        Assert.assertEquals(4, successor.key);
    }

    @Test
    public void testInsert() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);
        int[] expected = new int[]{2, 1, 4, 3, 6, 7, 5};
        // Act
        List<BinarySearchTree.TreeNode> nodesTraverse = new ArrayList<>();
        bst.inOrderTraverse(bst.head, nodesTraverse);
        // Assert
        Assert.assertArrayEquals(expected, getNodeKeys(nodesTraverse));
    }

    // Todo: implement
    @Test
    public void testDeleteFromTreeNodeExists() {
        // Arrange
        // Act
        // Assert
    }

    // Todo: implement
    @Test
    public void testDeleteFromTreeNodeNotExists() {
        // Arrange
        // Act
        // Assert
    }

    @Test
    public void testTransplantRoot() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);

        BinarySearchTree.TreeNode expectedNode = new BinarySearchTree.TreeNode(42);
        int[] expectedKeys = new int[]{42};
        // Act
        List<BinarySearchTree.TreeNode> nodes = new ArrayList<>();
        bst.transplant(bst.head, expectedNode);
        bst.inOrderTraverse(bst.head, nodes);
        // Assert
        Assert.assertEquals(expectedNode, bst.head);
        Assert.assertNull(expectedNode.parent);
        Assert.assertArrayEquals(expectedKeys, getNodeKeys(nodes));
    }

    @Test
    public void testTransplantNodeLeft() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);

        BinarySearchTree.TreeNode expectedNode = new BinarySearchTree.TreeNode(42);
        BinarySearchTree.TreeNode toTransplant = bst.head.left;
        int[] expectedKeys = new int[]{42, 6, 7, 5};
        // Act
        List<BinarySearchTree.TreeNode> nodes = new ArrayList<>();
        bst.transplant(toTransplant, expectedNode);
        bst.inOrderTraverse(bst.head, nodes);
        // Assert
        Assert.assertEquals(expectedNode, bst.head.left);
        Assert.assertEquals(expectedNode.parent, bst.head);
        Assert.assertArrayEquals(expectedKeys, getNodeKeys(nodes));
    }

    @Test
    public void testTransplantNodeRight() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);

        BinarySearchTree.TreeNode expectedNode = new BinarySearchTree.TreeNode(42);
        BinarySearchTree.TreeNode toTransplant = bst.head.left.right;
        int[] expectedKeys = new int[]{2, 1, 42, 3, 6, 7, 5};
        // Act
        List<BinarySearchTree.TreeNode> nodes = new ArrayList<>();
        bst.transplant(toTransplant, expectedNode);
        bst.inOrderTraverse(bst.head, nodes);
        // Assert
        Assert.assertEquals(expectedNode, bst.head.left.right);
        Assert.assertEquals(expectedNode.parent, bst.head.left);
        Assert.assertArrayEquals(expectedKeys, getNodeKeys(nodes));
    }

    @Test
    public void testTransplantNodeWithNull() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);

        BinarySearchTree.TreeNode toTransplant = bst.head.right;
        int[] expectedKeys = new int[]{2, 1, 4, 3, 5};
        // Act
        List<BinarySearchTree.TreeNode> nodes = new ArrayList<>();
        bst.transplant(toTransplant, null);
        bst.inOrderTraverse(bst.head, nodes);
        // Assert
        Assert.assertNull(null, bst.head.right);
        Assert.assertArrayEquals(expectedKeys, getNodeKeys(nodes));
    }

    @Test
    public void testDeleteNodeWithLeftChild() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);

        int[] expectedKeys = {2, 1, 4, 3, 6, 5};
        // Act
        List<BinarySearchTree.TreeNode> nodes = new ArrayList<>();
	    bst.delete(bst.head.right);
	    bst.inOrderTraverse(bst.head, nodes);
        // Assert
        Assert.assertArrayEquals(expectedKeys, getNodeKeys(nodes));
    }

    @Test
    public void testDeleteNodeWithRightChild() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);

        int[] expectedKeys = {2, 4, 3, 6, 7, 5};
        // Act
        List<BinarySearchTree.TreeNode> nodes = new ArrayList<>();
	    bst.delete(bst.head.left.left);
	    bst.inOrderTraverse(bst.head, nodes);
        // Assert
        Assert.assertArrayEquals(expectedKeys, getNodeKeys(nodes));
    }

    @Test
    public void testDeleteNodeWithBothChildrenImmediateRightChild() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        insertTestValues(bst);

        int[] expectedKeys = {2, 1, 4, 6, 7, 5};
        // Act
        List<BinarySearchTree.TreeNode> nodes = new ArrayList<>();
	    bst.delete(bst.head.left);
	    bst.inOrderTraverse(bst.head, nodes);
        // Assert
        Assert.assertArrayEquals(expectedKeys, getNodeKeys(nodes));
    }

    @Test
    public void testDeleteNodeWithBothChildrenDistantRightChild() {
        // Arrange
        BinarySearchTree bst = createTestTree();
        for (int i: new int[]{15, 20, 23, 18, 10, 13, 14, 12, 11, 5, 3, 8, 6, 1, 2}) bst.insert(new BinarySearchTree.TreeNode(i));

        int[] expectedKeys = new int[]{2, 1, 3, 6, 8, 5, 12, 14, 13, 11, 18, 23, 20, 15};
        // Act
        List<BinarySearchTree.TreeNode> nodes = new ArrayList<>();
        bst.delete(bst.head.left);
        bst.inOrderTraverse(bst.head, nodes);
        // Assert
        Assert.assertArrayEquals(expectedKeys, getNodeKeys(nodes));
    }

    private BinarySearchTree createTestTree() {
        return new BinarySearchTree();
    }

    private void insertTestValues(BinarySearchTree bst) {
        for (int i: new int[]{5, 3, 1, 2, 4, 7, 6}) bst.insert(new BinarySearchTree.TreeNode(i));
    }

    private int[] getNodeKeys(List<BinarySearchTree.TreeNode> nodes) {
        int i = -1;
        int[] keyNodes = new int[nodes.size()];
        for (BinarySearchTree.TreeNode node: nodes)
            keyNodes[++i] = node.key;
        return keyNodes;
    }
}
