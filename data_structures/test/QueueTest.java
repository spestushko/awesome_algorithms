import exceptions.QueueException;
import org.junit.Test;

import org.junit.Assert;

public class QueueTest {
    private final String[] queueArgs = new String[] {"Alicia", "Bob", "Marc"};

    @Test
    public void testInitQueue() {
        // Act
        Queue<String> queue = new Queue<>(3);
        // Assert
        Assert.assertNotNull(queue);
    }

    @Test
    public void testIsEmptyQueue() {
        // Arrange
        Queue<String> queue = new Queue<>(3);
        // Assert
        Assert.assertTrue(queue.isEmpty());
    }

    @Test
    public void testEnqueDequeue() throws QueueException {
        // Arrange
        Queue<String> queue = new Queue<>(3);
        // Act
        for (String q: queueArgs)
            queue.enque(q);
        // Assert
        for (String q: queueArgs)
            Assert.assertEquals(q, queue.deque());
    }

    @Test
    public void testEnqueDequeAboveCap() throws QueueException {
        // Arrange
        Queue<String> queue = new Queue<>(3);
        // Act
        for (String q: queueArgs)
            queue.enque(q);
        queue.enque("Morgan");
        queue.enque("Clark");
        // Assert
        for (String q: queueArgs)
            Assert.assertEquals(q, queue.deque());
        Assert.assertEquals("Morgan", queue.deque());
        Assert.assertEquals("Clark", queue.deque());
    }

    @Test (expected = QueueException.class)
    public void testQueueUnderflow() throws QueueException {
        // Arrange
        Queue<String> queue = new Queue<>(3);
        // Act
        queue.deque();
    }

}
