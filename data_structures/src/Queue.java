import exceptions.QueueException;

import java.util.Arrays;

public class Queue<E> {
    private int head = 0;
    private int tail = 0;
    private int size;
    private E[] data;

    Queue(int size) {
        this.size = size;
        this.data = (E[]) new Object[this.size];
    }

    private void ensureCapacity() {
        int resizeTo = size * 2;
        size = resizeTo;
        data = Arrays.copyOf(data, size);
    }

    boolean isEmpty() {
        return tail == 0;
    }

    private boolean isFull() {
        return tail == size;
    }

    Queue<E> enque(E e) {
        if (isFull())
            ensureCapacity();
        data[tail] = e;
        tail = tail == size ? 1 : ++tail;
        return this;
    }

    E deque() throws QueueException{
        if (isEmpty())
            throw new QueueException("Queue underflow");
        E d = data[head];
        head = head == size ? 1 : ++head;
        return d;
    }
}
