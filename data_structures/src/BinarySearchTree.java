import java.util.List;

// Todo: Implement as a generic


class BinarySearchTree {

    static class TreeNode {
		int key;
		boolean visited = false;
		TreeNode parent;
		TreeNode left;
		TreeNode right;

		TreeNode(int key) {
			this.key = key;
		}

		@Override
		public String toString() { return String.valueOf(key); }
	}

    TreeNode head;

    TreeNode minimum(TreeNode root) {
        if (root == null)
            return null;

        while (root.left != null)
            root = root.left;

        return root;
    }

    TreeNode maximum(TreeNode root) {
        if (root == null)
            return null;

        while (root.right != null)
            root = root.right;

        return root;
    }

    TreeNode successor(TreeNode root) {
        if (root == null)
            return null;

        if (root.right != null)
            return minimum(root.right);
        else {
            TreeNode p = root.parent;
            while (p != null && p.right == root) {
                root = p;
                p = p.parent;
            }
            return p;
        }
    }

    TreeNode predecessor(TreeNode root) {
        if (root == null)
            return null;

        if (root.left != null)
            return maximum(root.left);
        else {
            TreeNode p = root.parent;
            while (p != null && p.left == root) {
                root = p;
                p = p.parent;
            }
            return p;
        }
    }

    void insert(TreeNode element) {
        TreeNode parent = null;
        TreeNode iter = head;

        while(iter != null) {
            parent = iter;
            if (element.key < iter.key)
                iter = iter.left;
            else
                iter = iter.right;
        }

        if (parent == null) {
            head = element;
            return;
        }

        element.parent = parent;
        if (element.key < parent.key)
            parent.left = element;
        else
            parent.right = element;
    }

    TreeNode findTreeNode(TreeNode node, int key) {
        if (node == null)
            return null;

        if (node.key == key)
            return node;

        if (key < node.key)
            node = findTreeNode(node.left, key);
        else
            node = findTreeNode(node.right, key);

        return node;
    }

    int distanceToFromNode(TreeNode start, TreeNode end, int dist) {
        if (start == null)
            return -1;

        start.visited = true;

        if (start.key == end.key)
            return dist;

        int r = - 1;
        if (end.key < start.key) {
            if (start.left != null && !start.left.visited)
                r = distanceToFromNode(start.left, end, ++dist);
            if (r == -1)
                r = distanceToFromNode(start.parent, end, ++dist);
        } else {
            if (start.right != null && !start.right.visited)
                r = distanceToFromNode(start.right, end, ++dist);
            if (r == -1)
                r = distanceToFromNode(start.parent, end, ++dist);
        }

        return r;
    }

    int distance(TreeNode root, int v) {
        if (root.key == v)
            return 0;
        else if (v < root.key)
            return 1 + distance(root.left, v);
        return 1 + distance(root.right, v);
    }

    int distanceToFromRoot(TreeNode root, int node1, int node2) {
        if (root == null)
            return -1;

        if (root.key > node1 && root.key > node2)
            return distanceToFromRoot(root.left, node1, node2);

        if (root.key < node1 && root.key < node2)
            return distanceToFromRoot(root.right, node1, node2);

        if (root.key >= node1 && root.key <= node2)
            return distance(root, node1) + distance(root, node2);

        return -1;
    }

    void transplant(TreeNode oldNode, TreeNode newNode) {
        if (oldNode.parent == null)
            head = newNode;
        else if (oldNode == oldNode.parent.left)
            oldNode.parent.left = newNode;
        else
            oldNode.parent.right = newNode;

        if (newNode != null)
            newNode.parent = oldNode.parent;
    }

    void delete(TreeNode node) {
        if (node.left == null)
            transplant(node, node.right);
        else if (node.right == null)
            transplant(node, node.left);
        else {
            TreeNode newNode = minimum(node.right);
            if (newNode.parent != node) {
                transplant(newNode, newNode.right);
                newNode.right = node.right;
                newNode.right.parent = newNode;
            }
            transplant(node, newNode);
            newNode.left = node.left;
            newNode.left.parent = newNode;
        }
    }

    void inOrderTraverse(TreeNode root, List<TreeNode> nodes) {
        if (root == null)
            return;
        inOrderTraverse(root.left, nodes);
        inOrderTraverse(root.right, nodes);
        nodes.add(root);
    }
}
