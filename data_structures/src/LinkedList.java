class LinkedList<E> {

    static class Node<E> {
		private E key;
		private Node next;
		private Node prev;

		Node(E e) {
			key = e;
			next = null;
			prev = null;
		}

		void setNext(Node node) { this.next = node; }
		Node getNext() { return next; }

		void setPrev(Node node) { this.prev = node; }
		Node getPrev() { return prev; }

		E getKey() { return key; }
	}

    private Node head;
    private int size;

    LinkedList(E e) {
        head = new Node<>(e);
        size = 1;
    }

    void insert(E e) {
        Node newNode = new Node<>(e);
        newNode.setNext(head);
        head.setPrev(newNode);
        head = newNode;
        ++size;
    }

    Node find(E e) {
        Node iter = head;
        while (iter != null && iter.getKey() != e)
            iter = iter.getNext();
        return iter;
    }

    void delete(Node node) {
        if (node == null)
            return;

        if (node.getPrev() != null)
            node.getPrev().setNext(node.getNext());
        else
            head = node.getNext();

        if (node.getNext() != null)
            node.getNext().setPrev(node.getPrev());

        --size;
    }

    void print() {
        String[] data = new String[size];
        data = toArray(data);
        for (int i = 0; i < data.length; ++i)
            System.out.print("[".concat(String.valueOf(data[i])).concat("] -> "));
        System.out.println("\n");
    }

    <T> T[] toArray(T[] e) {
        Node iter = head;
        int i = -1;
        while (iter != null) {
            final T di = (T) iter.getKey();
            e[++i] = di;
            iter = iter.getNext();
        }
        return e;
    }
}
