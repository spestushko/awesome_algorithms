public class RedBlackBST {

	enum Color { BLACK, RED }

	private final static RBTreeNode nil = new RBTreeNode(-1);
	RBTreeNode head = nil;

	static class RBTreeNode {
		int key;
		Color color = Color.BLACK;
		RBTreeNode parent = nil;
		RBTreeNode left = nil;
		RBTreeNode right = nil;

		RBTreeNode (int key) { this.key = key; }

		@Override
		public String toString() { return String.valueOf(key); }
	}


	void leftRotate(RBTreeNode node) {
		if (node.right == nil)
			return;

		RBTreeNode pivot = node.right;
		node.right = pivot.left;
		if (pivot.left != nil)
			pivot.left.parent = node;
		pivot.parent = node.parent;
		if (node.parent == nil)
			head = pivot;
		else if (node == node.parent.left)
			node.parent.left = pivot;
		else
			node.parent.right = pivot;
		pivot.left = node;
		node.parent = pivot;
	}


	void rightRotate(RBTreeNode node) {
		if (node.left == nil)
			return;

		RBTreeNode pivot = node.left;
		node.left = pivot.right;
		if (pivot.right != nil)
			pivot.right.parent = node;
		pivot.parent = node.parent;
		if (node.parent == nil)
			head = pivot;
		else if (node == node.parent.left)
			node.parent.left = pivot;
		else
			node.parent.right = pivot;
		pivot.right = node;
		node.parent = pivot;
	}


	void insert(RBTreeNode node) {
		RBTreeNode parent = nil;
        RBTreeNode iter = head;

        while(iter != nil) {
            parent = iter;
            if (node.key < iter.key)
                iter = iter.left;
            else
                iter = iter.right;
        }

        if (parent == nil) {
            head = node;
            return;
        }

        node.parent = parent;
        if (node.key < parent.key)
            parent.left = node;
        else
            parent.right = node;

        node.left = nil;
        node.right = nil;
        node.color = Color.RED;
        insertFixup(node);
	}


	void insertFixup(RBTreeNode node) {
		// While red-black tree properties are violated
		while (node.parent != nil && node.parent.color == Color.RED) {
			// if node's parent is the left child of it's parent
			if (node.parent == node.parent.parent.left) {
				// Initialize pivot to node's cousin
				RBTreeNode pivot = node.parent.parent.right;
				// Recolor if node's cousin (pivot) is red
				if (pivot.color == Color.RED) {
					node.parent.color = Color.BLACK;
					pivot.color = Color.BLACK;
					node.parent.parent.color = Color.RED;
					node = node.parent.parent;
				// if pivot is black and node is a right child of it's parent
				} else if (node == node.parent.right){
						node = node.parent;
						leftRotate(node);
				// if pivot is black and node is a left child of it's parent
				} else {
					node.parent.color = Color.BLACK;
					node.parent.parent.color = Color.RED;
					rightRotate(node.parent.parent);
				}
			// if node's parent is the right child of it's parent
			} else {
				// Initialize pivot to node's cousin
				RBTreeNode pivot = node.parent.parent.left;
				// If pivot is red, recolor
				if (pivot.color == Color.RED) {
					node.parent.color = Color.BLACK;
					pivot.color = Color.BLACK;
					node.parent.parent.color = Color.RED;
					node = node.parent.parent;
				// If pivot is black and node is a left child of it's parent
				} else if (node == node.parent.left) {
						node = node.parent;
						rightRotate(node);
				// If pivot is black and node is a right child of it's parent
				} else {
					node.parent.color = Color.BLACK;
					node.parent.parent.color = Color.RED;
					leftRotate(node.parent.parent);
				}
			}
		}

		// Root is always black
		head.color = Color.BLACK;
	}

	void transplant(RBTreeNode oldNode, RBTreeNode newNode) {
        if (oldNode.parent == nil)
            head = newNode;
        else if (oldNode == oldNode.parent.left)
            oldNode.parent.left = newNode;
        else
            oldNode.parent.right = newNode;
        newNode.parent = oldNode.parent;
	}

	RBTreeNode minimum(RBTreeNode root) {
		if (root == nil)
			return null;

		while (root.left != nil)
			root = root.left;

		return root;
	}

	public void delete(RBTreeNode node) {
		RBTreeNode volatileNode = node;
		RBTreeNode childRef = nil;
		Color volatileNodeColor = volatileNode.color;

		if (node.left == nil) {
			childRef = node.right;
			transplant(node, node.right);
		} else if (node.right == nil) {
			childRef = node.left;
			transplant(node, node.left);
		} else {
			volatileNode = minimum(node.right);
			volatileNodeColor = volatileNode.color;
			childRef = volatileNode.right;

			if (volatileNode.parent == node)
				childRef.parent = volatileNode;
			else {
				transplant(volatileNode, volatileNode.right);
				volatileNode.right = node.right;
				volatileNode.right.parent = volatileNode;
			}

			transplant(node, volatileNode);
			volatileNode.left = node.left;
			volatileNode.left.parent = volatileNode;
			volatileNode.color = node.color;
		}

		if (volatileNodeColor == Color.BLACK)
			deleteFixup(childRef);
	}


	void deleteFixup(RBTreeNode node) {
		while (node != head && node.color == Color.BLACK) {
			if (node == node.parent.left) {
				RBTreeNode childRef = node.parent.right;
				// Case 1
				if (childRef.color == Color.RED) {
					childRef.color = Color.BLACK;
					node.parent.color = Color.RED;
					leftRotate(node.parent);
					childRef = node.parent.right;
				}
				// Case 2
				if (childRef.left.color == Color.BLACK && childRef.right.color == Color.BLACK) {
					childRef.color = Color.RED;
					node = node.parent;
				// Case 3
				} else if (childRef.right.color == Color.BLACK) {
					childRef.left.color = Color.BLACK;
					childRef.color = Color.RED;
					rightRotate(childRef);
					childRef = node.parent.right;
				// Case 4
				} else {
					childRef.color = node.parent.color;
					node.parent.color = Color.BLACK;
					childRef.right.color = Color.BLACK;
					leftRotate(node.parent);
					node = head;
				}
			} else {
				// Same as then with right and left exchanged
				RBTreeNode childRef = node.parent.left;
				// Case 1
				if (childRef.color == Color.RED) {
					childRef.color = Color.BLACK;
					node.parent.color = Color.RED;
					rightRotate(node.parent);
					childRef = node.parent.left;
				}
				// Case 2
				if (childRef.right.color == Color.BLACK && childRef.left.color == Color.BLACK) {
					childRef.color = Color.RED;
					node = node.parent;
				// Case 3
				} else if (childRef.left.color == Color.BLACK) {
					childRef.right.color = Color.BLACK;
					childRef.color = Color.RED;
					leftRotate(childRef);
					childRef = node.parent.left;
				// Case 4
				} else {
					childRef.color = node.parent.color;
					node.parent.color = Color.BLACK;
					childRef.left.color = Color.BLACK;
					rightRotate(node.parent);
					node = head;
				}
			}
		}

		node.color = Color.BLACK;
	}
}
