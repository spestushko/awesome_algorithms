import exceptions.StackException;

import java.util.Arrays;

class Stack<E> {
    private E[] data;
    private int top = 0;
    private int size;

    Stack(int size) {
        this.size = size;
        data = (E[]) new Object[this.size];
    }

    private void ensureCapacity() {
        int resizeTo = size * 2;
        data = Arrays.copyOf(data, resizeTo);
        size = resizeTo;
    }

    boolean isEmpty() {
        return top == 0;
    }

    Stack<E> push(E e) {
        if (top == size)
            ensureCapacity();
        data[top++] = e;
        return this;
    }

    E pop() throws StackException{
        if (top == 0)
            throw new StackException("Stack underflow");
        return data[--top];
    }
}
