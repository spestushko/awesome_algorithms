import org.junit.Assert;
import org.junit.Test;

public class VideoShotSequenceTest {
    
    @Test
    public void test3LongSceneSegment() {
        // Arrange
        char[] shots = new char[] {
                'a','b','a','b','c','b','a','c','a',
                'd', 'e','f','e','g','d','e',
                'h','i','j','h','k','l','i','j'};
        int[] expected = new int[]{9, 7, 8};
        // Act
        int[] actual = VideoShotSequence.findScenes(shots);
        // Assert
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void test3SingleSceneSegment() {
        // Arrange
        char[] shots = new char[] {'a','b','c'};
        int[] expected = new int[]{1, 1, 1};
        // Act
        int[] actual = VideoShotSequence.findScenes(shots);
        // Assert
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testSingleSceneSegment() {
        // Arrange
        char[] shots = new char[] {'a','b','c', 'a'};
        int[] expected = new int[]{4};
        // Act
        int[] actual = VideoShotSequence.findScenes(shots);
        // Assert
        Assert.assertArrayEquals(expected, actual);
    }
    
}
