import org.junit.Test;
import org.junit.Assert;

public class MatchingBracketsTest {

    @Test
    public void testUnbalancedString() {
        // Arrange
        String s = "(h[e{|<|>o}!]~)()()()(";
        int expected = 0;
        // Act
        int actual = MatchingBrackets.isBalanced(s);
        // Assert
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testBalancedString() {
        // Arrange
        String s = "[](){}<>";
        int expected = 1;
        // Act
        int actual = MatchingBrackets.isBalanced(s);
        // Assert
        Assert.assertEquals(expected, actual);
    }

}
