import org.junit.Assert;
import org.junit.Test;

public class TwoSumTest {

   @Test
   public void testTwoSumBrute() {
      // Arrange
      int[] nums = new int[]{2, 7, 11, 15};
      int target = 9;
      int[] expected = new int[]{0, 1};
      // Act
      int[] actual = TwoSum.findBrute(nums, target);
      // Assert
      Assert.assertArrayEquals(expected, actual);
   }

   @Test
   public void testTwoSum2Brute() {
      // Arrange
      int[] nums = new int[]{3, 2, 4};
      int target = 6;
      int[] expected = new int[]{1, 2};
      // Act
      int[] actual = TwoSum.findBrute(nums, target);
      // Assert
      Assert.assertArrayEquals(expected, actual);
   }

   @Test
   public void testTwoSum3Brute() {
      // Arrange
      int[] nums = new int[]{3, 3};
      int target = 6;
      int[] expected = new int[]{0, 1};
      // Act
      int[] actual = TwoSum.findBrute(nums, target);
      // Assert
      Assert.assertArrayEquals(expected, actual);
   }

   @Test
   public void testTwoSumBruteNull() {
      // Arrange
      int[] nums = null;
      int target = 6;
      int[] expected = new int[]{0, 0};
      // Act
      int[] actual = TwoSum.findBrute(nums, target);
      // Assert
      Assert.assertArrayEquals(expected, actual);
   }

   @Test
   public void testTwoSumOptimal() {
      // Arrange
      int[] nums = new int[]{2, 7, 11, 15};
      int target = 9;
      int[] expected = new int[]{0, 1};
      // Act
      int[] actual = TwoSum.findOptimal(nums, target);
      // Assert
      Assert.assertArrayEquals(expected, actual);
   }

   @Test
   public void testTwoSum2Optimal() {
      // Arrange
      int[] nums = new int[]{3, 2, 4};
      int target = 6;
      int[] expected = new int[]{1, 2};
      // Act
      int[] actual = TwoSum.findOptimal(nums, target);
      // Assert
      Assert.assertArrayEquals(expected, actual);
   }

   @Test
   public void testTwoSum3Optimal() {
      // Arrange
      int[] nums = new int[]{3, 3};
      int target = 6;
      int[] expected = new int[]{0, 1};
      // Act
      int[] actual = TwoSum.findOptimal(nums, target);
      // Assert
      Assert.assertArrayEquals(expected, actual);
   }

   @Test
   public void testTwoSumNullOptimal() {
      // Arrange
      int[] nums = null;
      int target = 6;
      int[] expected = new int[]{0, 0};
      // Act
      int[] actual = TwoSum.findOptimal(nums, target);
      // Assert
      Assert.assertArrayEquals(expected, actual);
   }

}
