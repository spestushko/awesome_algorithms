import org.junit.Assert;
import org.junit.Test;

public class Problem1Test {

    @Test
    public void test1() {
        // Arrange
        int[] n = new int[]{1, 2, 3, 4, 5};
        // Act
	    Problem1.solve();
        // Assert
	    Assert.assertEquals(1, 1);
    }

}
