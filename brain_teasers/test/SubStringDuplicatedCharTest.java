import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SubStringDuplicatedCharTest {

    @Test
    public void testFindSubstring() {
        // Arrange
        String s = "awaglk";
        int k = 4;
        List<String> expected = new ArrayList<>();
        expected.add("awag");
        // Act
        List<String> result = SubStringDuplicatedChar.find(s, k);
        // Assert
        Assert.assertTrue(expected.equals(result));
    }

    @Test
    public void testFindSubstringNotExist() {
        // Arrange
        String s = "awbglk";
        int k = 4;
        List<String> expected = new ArrayList<>();
        // Act
        List<String> result = SubStringDuplicatedChar.find(s, k);
        // Assert
        Assert.assertTrue(expected.equals(result));
    }

}
