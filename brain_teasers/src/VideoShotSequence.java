import java.util.ArrayList;
import java.util.List;

public class VideoShotSequence {

   static int[] findScenes(char[] shots) {
      List<List<Character>> scenes = new ArrayList<>();

      for (int i = 0; i < shots.length; ++i) {
         char shot = shots[i];
         List<Character> scene = new ArrayList<>();
         scene.add(shot);

         int firstSeenIn = lookupShotInScene(scenes, shot);
         scenes.add(scene);
         if (firstSeenIn != -1) {
            collapseScenes(scenes, firstSeenIn, scenes.size() - 1);
         }
      }

      int[] sceneLengths = new int[scenes.size()];
      for (int i = 0; i < scenes.size(); ++i) {
         sceneLengths[i] = scenes.get(i).size();
      }
      return sceneLengths;
   }

   static int lookupShotInScene(List<List<Character>> scenes, Character shot) {
      int idx = -1;
      for (List<Character> scene: scenes) {
         idx++;
         if (scene.contains(shot))
            return idx;
      }
      return -1;
   }

   static void collapseScenes(List<List<Character>> scenes, int from, int to) {
      // Merges all scenes into 1
      for (int i = to; i > from; --i) {
         List<Character> merge = scenes.get(i);
         scenes.get(from).addAll(merge);
         scenes.remove(i);
      }
   }


}
