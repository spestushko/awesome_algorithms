import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Todo: Improve this
class SubStringDuplicatedChar {

    static List<String> find(String s, int k) {
        List<String> result = new ArrayList<>();
        int p = 0;
        int q = k - 1;

        while (q <= s.length() - 1) {
            Map<Character, Integer> occurences = new HashMap<>();
            Character dupl = null;
            boolean valid = false;

            for (int i = p; i <= q; ++i) {
                Character si = s.charAt(i);

                if (!occurences.containsKey(si))
                    occurences.put(si, 0);
                int times = occurences.get(si);
                occurences.put(si, ++times);

                if (times == 2 && dupl == null) {
                    dupl = si;
                    valid = true;
                }
            }

            if (valid)
                result.add(s.substring(p, q + 1));

            p++;
            q++;
        }

        return result;
    }

}
