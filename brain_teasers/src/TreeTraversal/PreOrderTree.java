package TreeTraversal;

public class PreOrderTree {

	static void traverse(Node root) {
		if (root == null)
			return;
		System.out.println("> " + root.data);
		traverse(root.left);
		traverse(root.right);
	}

	public static void main(String args[]){
		Node root = new Node(1);
		root.left = new Node(2);
		root.right = new Node(3);
		root.left.left = new Node(4);
		root.left.right = new Node(5);

		traverse(root);
	}

}
