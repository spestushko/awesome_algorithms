package TreeTraversal;

import java.util.Stack;

public class DFSTree {

	static void traverse(Node root) {
		if (root == null)
			return;

		Stack<Node> stack = new Stack<>();
		stack.add(root);

		while (!stack.isEmpty()) {
			Node node = stack.pop();
			System.out.println("> " + node.data);
			if (node.left != null)
				stack.add(node.left);
			if (node.right != null)
				stack.add(node.right);
		}
	}

	public static void main(String args[]){
		Node root = new Node(1);
		root.left = new Node(2);
		root.left.left = new Node(4);
		root.left.right = new Node(5);
		root.right = new Node(3);
		root.right.left = new Node(6);
		root.right.right = new Node(7);

		DFSTree.traverse(root);
	}
}
