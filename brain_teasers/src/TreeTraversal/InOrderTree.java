package TreeTraversal;

public class InOrderTree {

	static void traverse(Node root) {
		if (root == null)
			return;
		traverse(root.left);
		System.out.println("> " + root.data);
		traverse(root.right);
	}

	public static void main(String args[]){
		Node root = new Node(1);
		root.left = new Node(2);
		root.right = new Node(3);
		root.left.left = new Node(4);
		root.left.right = new Node(5);

		traverse(root);
	}
}
