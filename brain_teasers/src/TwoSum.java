import java.util.LinkedHashMap;
import java.util.Map;

class TwoSum {

   static int[] findOptimal(int[] nums, int target) {
      if (nums == null || nums.length < 2)
          return new int[]{0, 0};

      Map<Integer, Integer> sumMap = new LinkedHashMap<>();
      for (int i = 0; i < nums.length; ++i) {
          if (sumMap.containsKey(nums[i]))
             return new int[]{sumMap.get(nums[i]), i};
          else
             sumMap.put(target - nums[i], i);
      }
      return new int[]{0, 0};
   }

   static int[] findBrute(int[] nums, int target) {
       if (nums == null || nums.length < 2)
          return new int[]{0, 0};

       for (int i = 0; i < nums.length - 1; ++i)
           for (int j = i + 1; j < nums.length; ++j)
               if (nums[i] + nums[j] == target)
                   return new int[]{i, j};
       return new int[]{0, 0};
   }
}
