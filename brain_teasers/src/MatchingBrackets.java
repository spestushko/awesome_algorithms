import java.util.HashMap;
import java.util.Stack;

class MatchingBrackets {

   static int isBalanced(String s) {
      Stack<Character> brackets = new Stack<>();
      HashMap<Character, Character> charMap = new HashMap<>();
      charMap.put('(', ')');
      charMap.put('{', '}');
      charMap.put('[', ']');
      charMap.put('<', '>');

      for (int i = 0; i < s.length(); ++i) {
         Character si = s.charAt(i);
         if (charMap.containsKey(si)) brackets.add(si);
         if (charMap.containsValue(si)) brackets.pop();
      }

      return brackets.size() == 0 ? 1 : 0;
   }

}
