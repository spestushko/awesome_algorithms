class MinPriorityQueue {

    private int[] a;
    private int heapSize;

    MinPriorityQueue(int[] a) {
        this.a = a.clone();
        heapSize = this.a.length;
    }

    int minimum() { return a[0]; }

    int extract() {
        if (heapSize < 1)
            throw new IllegalArgumentException("Heap underflow");
        int min = minimum();
        a[0] = a[heapSize-1];
        this.heapSize -= 1;
        Heap.minHeapify(a, heapSize-1, 0);
        return min;
    }

    void decreaseKey(int i, int key) {
        if (key > a[i])
            throw new IllegalArgumentException("New key is bigger than current key");
        a[i] = key;
        while (i > 0 && a[Heap.parent(i)] > a[i]) {
            Helpers.swap(a, i, Heap.parent(i));
            i = Heap.parent(i);
        }
    }

    void insert(int key) {
        heapSize += 1;
        int[] tmp = a.clone();
        a = new int[heapSize];
        System.arraycopy(tmp, 0, a, 0, tmp.length);
        a[heapSize - 1] = Integer.MAX_VALUE;
        decreaseKey(heapSize - 1, key);
    }

    int[] getHeap() { return a; }
}
