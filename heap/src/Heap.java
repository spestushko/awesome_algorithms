import java.util.Arrays;

public class Heap {

    static int parent(int i) { return i == 0 ? 0 : (i-1) / 2; }
    static int left(int i) { return 2 * i + 1; }
    static int right(int i) {
        return 2 * i + 2;
    }


    static void maxHeapify(int[] a, int heapSize, int i) {
        int l = left(i);
        int r = right(i);

        int largest = l < heapSize && a[l] > a[i] ? l : i;
        largest = r < heapSize && a[r] > a[largest] ? r : largest;

        if (largest != i) {
            Helpers.swap(a, i, largest);
            maxHeapify(a, heapSize, largest);
        }
    }


    static void buildMaxHeap(int[] a) {
        int n = a.length;
        for (int i = parent(n); i >= 0; i--)
            maxHeapify(a, n, i);
    }


    static void heapSortAsc(int[] a) {
        buildMaxHeap(a);
        int n = a.length - 1;
        for (int i = n; i >= 1; i--) {
            Helpers.swap(a, 0, i);
            maxHeapify(a, i, 0);
        }
    }


    static void minHeapify(int[] a, int heapSize, int i) {
        int l = left(i);
        int r = right(i);

        int smallest = l < heapSize && a[l] < a[i] ? l : i;
        smallest = r < heapSize && a[r] < a[smallest] ? r : smallest;

        if (smallest != i) {
            Helpers.swap(a, i, smallest);
            minHeapify(a, heapSize, smallest);
        }
    }


    static void buildMinHeap(int[] a) {
        int n = a.length;
        for (int i = parent(n); i >= 0; i--)
            minHeapify(a, n, i);
    }


    static void heapSortDesc(int[] a) {
        buildMinHeap(a);
        int n = a.length - 1;
        for (int i = n; i >= 1; i--) {
            Helpers.swap(a, 0, i);
            minHeapify(a, i, 0);
        }
    }


    public static void main(String[] args) { }
}
