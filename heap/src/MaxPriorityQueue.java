class MaxPriorityQueue {

    private int[] a;
    private int heapSize;

    MaxPriorityQueue(int[] a) {
        this.a = a.clone();
        heapSize = this.a.length;
    }

    int maximum() {
        return a[0];
    }

    int extract() {
        if (heapSize < 1)
            throw new IllegalArgumentException("Heap underflow");
        int max = maximum();
        a[0] = a[heapSize-1];
        this.heapSize -= 1;
        Heap.maxHeapify(a, heapSize-1, 0);
        return max;
    }

    void increaseKey(int i, int key) {
        if (key < a[i])
            throw new IllegalArgumentException("New key is smaller than current key");
        a[i] = key;
        while (i > 0 && a[Heap.parent(i)] < a[i]) {
            Helpers.swap(a, i, Heap.parent(i));
            i = Heap.parent(i);
        }
    }

    void insert(int key) {
        heapSize += 1;
        int[] tmp = a.clone();
        a = new int[heapSize];
        System.arraycopy(tmp, 0, a, 0, tmp.length);
        a[heapSize - 1] = Integer.MIN_VALUE;
        increaseKey(heapSize - 1, key);
    }

    int[] getHeap() {
        return a;
    }

}