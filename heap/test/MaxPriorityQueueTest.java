import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class MaxPriorityQueueTest {

    @Test
    public void testPriorityQueueMaximum() {
        // Arrange
        int[] a = new int[]{1, 2, 3, 4, 5};
        MaxPriorityQueue mpq = new MaxPriorityQueue(a);

        // Act
        int max = mpq.maximum();

        // Assert
        assertEquals(1, max);
    }

    @Test
    public void testPriorityQueueExtract() {
        // Arrange
        int[] a = new int[]{16, 14, 10, 8, 7, 9, 3, 2, 4, 1};
        int maxExpected1 = a[0];
        int maxExpected2 = a[1];
        MaxPriorityQueue mpq = new MaxPriorityQueue(a);

        // Act
        int extract1 = mpq.extract();
        int extract2 = mpq.extract();

        // Assert
        assertEquals(maxExpected1, extract1);
        assertEquals(maxExpected2, extract2);
    }

    @Test
    public void testIncreaseKey() {
        // Arrange
        int[] a = new int[]{16, 14, 10, 8, 7, 9, 3, 2, 4, 1};
        int[] e = new int[]{17, 14, 16, 8, 7, 9, 10, 2, 4, 1};
        int inc = 17;
        int at = 6;

        // Act
        MaxPriorityQueue maxPriorityQueue = new MaxPriorityQueue(a);
        maxPriorityQueue.increaseKey(at, inc);

        // Assert
        assertArrayEquals(e, maxPriorityQueue.getHeap());
    }

    @Test
    public void testInsert() {
        // Arrange
        int[] a = new int[]{16, 14, 10, 8, 7, 9, 3, 2, 4, 1};
        int[] e = new int[]{16, 14, 10, 8, 12, 9, 3, 2, 4, 1, 7};

        // Act
        MaxPriorityQueue maxPriorityQueue = new MaxPriorityQueue(a);
        maxPriorityQueue.insert(12);

        // Assert
        assertArrayEquals(e, maxPriorityQueue.getHeap());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExtractEmptyArray() {
        // Arrange
        int[] a = new int[]{};
        MaxPriorityQueue maxPriorityQueue = new MaxPriorityQueue(a);

        // Act
        maxPriorityQueue.extract();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseKeySmallerValue() {
        // Arrange
        int[] a = new int[]{16, 14, 10, 8, 7, 9, 3, 2, 4, 1};
        int at = 0;
        int inc = 15;
        MaxPriorityQueue maxPriorityQueue = new MaxPriorityQueue(a);

        // Act
        maxPriorityQueue.increaseKey(at, inc);
    }
}
