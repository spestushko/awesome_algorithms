import org.junit.Test;
import static org.junit.Assert.*;


public class HeapTest {


    @Test
    public void testParentCalculationOdd() {
        assertEquals(4, Heap.parent(9));
    }


    @Test
    public void testParentCalculationEven() {
        assertEquals(2, Heap.parent(6));
    }


    @Test
    public void testLeftCalculationEven() {
        assertEquals(9, Heap.left(4));
    }


    @Test
    public void testLeftCalculationOdd() {
        assertEquals(11, Heap.left(5));
    }


    @Test
    public void testRightCalculationEven() {
        assertEquals(10, Heap.right(4));
    }


    @Test
    public void testRightCalculationOdd() {
        assertEquals(12, Heap.right(5));
    }


    @Test
    public void testMaxHeapify() {
        int[] a = new int[]{16, 4, 10, 14, 7, 9, 3, 2, 8, 1};
        int[] e = new int[]{16, 14, 10, 8, 7, 9, 3, 2, 4, 1};

        Heap.maxHeapify(a, a.length, 1);

        assertArrayEquals(e, a);
        assertTrue(HeapTest.isMaxHeap(a, 0, a.length - 1));
    }

    @Test
    public void testMinHeapify() {
        int[] a = new int[]{4, 1, 3, 8, 7, 9, 10, 14, 8, 16};
        int[] e = new int[]{1, 4, 3, 8, 7, 9, 10, 14, 8, 16};

        Heap.minHeapify(a, a.length, 0);

        assertArrayEquals(e, a);
        assertTrue(HeapTest.isMinHeap(a, 0, a.length - 1));
    }


    @Test
    public void testBuildMaxHeap() {
        int[] a = new int[]{1, 2, 3, 4, 7, 8, 9, 10, 14, 16};
        int[] e = new int[]{16, 14, 9, 10, 7, 8, 3, 1, 4, 2};

        Heap.buildMaxHeap(a);

        assertArrayEquals(e, a);
        assertTrue(HeapTest.isMaxHeap(a, 0, a.length - 1));
    }


    @Test
    public void testBuildMinHeap() {
        int[] a = new int[]{4, 1, 3, 2, 16, 9, 10, 14, 8, 7};
        int[] e = new int[]{1, 2, 3, 4, 7, 9, 10, 14, 8, 16};

        Heap.buildMinHeap(a);

        assertArrayEquals(e, a);
        assertTrue(HeapTest.isMinHeap(a, 0, a.length - 1));
    }


    @Test
    public void testHeapSortAsc() {
        int[] a = new int[]{4, 1, 3, 2, 16, 9, 10, 14, 8, 7};
        int[] e = new int[]{1, 2, 3, 4, 7, 8, 9, 10, 14, 16};

        Heap.heapSortAsc(a);

        assertArrayEquals(e, a);
    }


    @Test
    public void testHeapSortDesc() {
        int[] a = new int[]{4, 1, 3, 2, 16, 9, 10, 14, 8, 7};
        int[] e = new int[]{16, 14, 10, 9, 8, 7, 4, 3, 2, 1};

        Heap.heapSortDesc(a);

        assertArrayEquals(e, a);
    }


    private static boolean isMaxHeap(int[] arr, int i, int n) {
        if (i > (n - 2) / 2)
            return true;
        return  arr[i] >= arr[2 * i + 1] && arr[i] >= arr[2 * i + 2] &&
                isMaxHeap(arr, 2 * i + 1, n) && isMaxHeap(arr, 2 * i + 2, n);
    }


    private static boolean isMinHeap(int[] arr, int i, int n) {
        if (i > (n - 2) / 2)
            return true;
        return  arr[i] <= arr[2 * i + 1] && arr[i] <= arr[2 * i + 2] &&
                isMinHeap(arr, 2 * i + 1, n) && isMinHeap(arr, 2 * i + 2, n);
    }
}
