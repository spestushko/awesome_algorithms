import org.junit.Test;
import static org.junit.Assert.*;

public class MinPriorityQueueTest {

    @Test
    public void testPriorityQueueMinimum() {
        // Arrange
        int[] a = new int[]{1, 2, 3, 4, 5};
        MinPriorityQueue mpq = new MinPriorityQueue(a);

        // Act
        int min = mpq.minimum();

        // Assert
        assertEquals(1, min);
    }

    @Test
    public void testPriorityQueueExtract() {
        // Arrange
        int[] a = new int[]{1, 2, 3, 4, 7, 9, 10, 14, 8, 16};
        int minExpected1 = a[0];
        int minExpected2 = a[1];
        MinPriorityQueue mpq = new MinPriorityQueue(a);

        // Act
        int extract1 = mpq.extract();
        int extract2 = mpq.extract();

        // Assert
        assertEquals(minExpected1, extract1);
        assertEquals(minExpected2, extract2);
    }

    @Test
    public void testIncreaseKey() {
        // Arrange
        int[] a = new int[]{1, 2, 3, 4, 7, 9, 10, 14, 8, 16};
        int[] e = new int[]{1, 2, 3, 4, 5, 9, 10, 14, 8, 7};
        int dec = 5;
        int at = 9;

        // Act
        MinPriorityQueue minPriorityQueue = new MinPriorityQueue(a);
        minPriorityQueue.decreaseKey(at, dec);

        // Assert
        assertArrayEquals(e, minPriorityQueue.getHeap());
    }

    @Test
    public void testInsert() {
        // Arrange
        int[] a = new int[]{1, 2, 3, 4, 7, 9, 10, 14, 8, 16};
        int[] e = new int[]{1, 2, 3, 4, 5, 9, 10, 14, 8, 16, 7};

        // Act
        MinPriorityQueue minPriorityQueue = new MinPriorityQueue(a);
        minPriorityQueue.insert(5);

        // Assert
        assertArrayEquals(e, minPriorityQueue.getHeap());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExtractEmptyArray() {
        // Arrange
        int[] a = new int[]{};
        MinPriorityQueue minPriorityQueue = new MinPriorityQueue(a);

        // Act
        minPriorityQueue.extract();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseKeySmallerValue() {
        // Arrange
        int[] a = new int[]{16, 14, 10, 8, 7, 9, 3, 2, 4, 1};
        int at = 0;
        int dec = 20;
        MinPriorityQueue minPriorityQueue = new MinPriorityQueue(a);

        // Act
        minPriorityQueue.decreaseKey(at, dec);
    }
}
