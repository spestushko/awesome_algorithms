import java.util.Arrays;
import static java.lang.System.*;

public class Main {

    public static class MergeSort {

        static void mergeSentinel(int[] a, int l, int m, int r) {
            int ls = m - l + 1; // +1 because m index is included
            int rs = r - m;
            int[] la = new int[ls+1]; // +1 to account for sentinel value
            int[] ra = new int[rs+1]; // same as above

            for (int i = 0; i < ls; ++i)
                la[i] = a[l + i];
            for (int j = 0; j < rs; ++j)
                ra[j] = a[m + j + 1]; // +1 to go past a[m]

            la[ls] = Integer.MAX_VALUE;
            ra[rs] = Integer.MAX_VALUE;

            int i = 0;
            int j = 0;
            for (int k = l; k <= r; ++k) {
                if (la[i] <= ra[j]) {
                    a[k] = la[i];
                    i = i + 1;
                } else {
                    a[k] = ra[j];
                    j = j + 1;
                }
            }
        }

        static void mergeCopyAll(int a[], int l, int m, int r) {
            int ls = m - l + 1;
            int rs = r - m;
            int[] la = new int[ls];
            int[] ra = new int[rs];

            for (int i = 0; i < ls; ++i)
                la[i] = a[l + i];
            for (int j = 0; j < rs; ++j)
                ra[j] = a[m + j + 1]; // +1 to go past a[m]

            int k = l;
            int i = 0;
            int j = 0;

            while (k < a.length) {
                if (la[i] <= ra[j]) {
                    a[k] = la[i];
                    k++;
                    i++;

                    // When reached end of the left array
                    if (i == la.length) {
                        // Copy remainder of the right array to target array
                        while (j < ra.length) {
                            a[k] = ra[j];
                            k++;
                            j++;
                        }
                        break;
                    }
                } else {
                    a[k] = ra[j];
                    k++;
                    j++;

                    // When reached end of the right array
                    if (j == ra.length) {
                        // Copy remainder of the left array to target array
                        while (i < la.length) {
                            a[k] = la[i];
                            k++;
                            i++;
                        }
                        break;
                    }
                }
            }
        }

        static int[] sort(int[] a, int l, int r) {
            if (l < r) {
                int m = (l + r) / 2;
                sort(a, l, m);
                sort(a, m+1, r);
                mergeCopyAll(a, l, m, r);
            }
            return a;
        }

    }

    public static void main(String[] args) {
        int[] testData = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        out.println("\nMERGE SORT\n");
        out.println("Before sort: ".concat(Arrays.toString(testData)));
        out.println("↑After sort: ".concat(Arrays.toString(MergeSort.sort(testData, 0, testData.length - 1))));
    }

}
