import java.util.Arrays;

class Main {
	public static int[] addBitIntegers(int[] a, int[] b) {
		int[] c = new int[a.length+1];
		int carry = 0;

		for (int i = a.length; i > 0; --i) {
			int sum = a[i-1] + b[i-1] + carry;
			c[i] = sum % 2;
			carry = sum / 2;
		}

		c[0] = carry;
		return c;
	}

	public static void main(String[] args) {
		int[] a = new int[]{1, 0, 1, 1};
		int[] b = new int[]{0, 0, 1, 1};
		System.out.println(Arrays.toString(Main.addBitIntegers(a, b)));
	}
}