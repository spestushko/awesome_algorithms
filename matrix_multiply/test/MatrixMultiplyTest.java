import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MatrixMultiplyTest {

    private final int n = 2;
    private final int[][] a = new int[n][n];
    private final int[][] b = new int[n][n];
    private final int[][] c = new int[n][n];

    @Before
    public void setup() {
        a[0][0] = 5;
        a[0][1] = 8;
        a[1][0] = 3;
        a[1][1] = 8;

        b[0][0] = 3;
        b[0][1] = 8;
        b[1][0] = 8;
        b[1][1] = 9;

        c[0][0] = 79;
        c[0][1] = 112;
        c[1][0] = 73;
        c[1][1] = 96;
    }

    @Test
    public void testCubicArrayMultiplication() {

        // Act
        int[][] actual = MatrixMultiply.Cubic.multiply(a, b);

        // Assert
        assertTrue(Arrays.deepEquals(c, actual));
    }

    @Test
    public void testCubicArrayMultiplication4x4() {
        // Arrange
        int[][] a4x4 = new int[][] {
            {5, 7, 9, 10},
            {2, 3, 3, 8},
            {8, 10, 2, 3},
            {3, 3, 4, 8}
        };

        int[][] b4x4 = new int[][] {
            {3, 10, 12, 18},
            {12, 1, 4, 9},
            {9, 10, 12, 2},
            {3, 12, 4, 10}
        };

        int[][] c4x4 = new int[][] {
            {210, 267, 236, 271},
            {93, 149, 104, 149},
            {171, 146, 172, 268},
            {105, 169, 128, 169}
        };

        // Act
        int[][] actual4x4 = MatrixMultiply.Cubic.multiply(a4x4, b4x4);

        // Assert
        assertTrue(Arrays.deepEquals(c4x4, actual4x4));
    }

    @Test
    public void testFaultyCubicArrayMultiplication() {

        c[1][1] = 42;

        // Act
        int[][] actual = MatrixMultiply.Cubic.multiply(a, b);

        // Assert
        assertFalse(Arrays.deepEquals(c, actual));
    }

    @Test
    public void testDivNCnqMultiplication() {

        // Act
        int[][] actual = MatrixMultiply.DivNCnq.multiply(a, b, 0, 0, 0,0, n);

        // Assert
        assertTrue(Arrays.deepEquals(c, actual));
    }

    @Test
    public void testFaultyDivNCnqMultiplication() {

        // Arrange
        c[1][1] = 42;

        // Act
        int[][] actual = MatrixMultiply.DivNCnq.multiply(a, b, 0, 0, 0,0, n);

        // Assert
        assertFalse(Arrays.deepEquals(c, actual));
    }

    @Test
    public void testDivNCnqMultiplication4x4() {
        // Arrange
        int[][] a4x4 = new int[][] {
            {5, 7, 9, 10},
            {2, 3, 3, 8},
            {8, 10, 2, 3},
            {3, 3, 4, 8}
        };

        int[][] b4x4 = new int[][] {
            {3, 10, 12, 18},
            {12, 1, 4, 9},
            {9, 10, 12, 2},
            {3, 12, 4, 10}
        };

        int[][] c4x4 = new int[][] {
            {210, 267, 236, 271},
            {93, 149, 104, 149},
            {171, 146, 172, 268},
            {105, 169, 128, 169}
        };

        // Act
        int[][] actual4x4 = MatrixMultiply.DivNCnq.multiply(a4x4, b4x4, 0, 0, 0, 0, 4);

        // Assert
        assertTrue(Arrays.deepEquals(c4x4, actual4x4));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testFaultyDimensionDivNCnqMultiplication4x4() {
        // Arrange
        int[][] a4x4 = new int[][] {
            {5, 7, 9, 10},
            {2, 3, 3, 8},
            {8, 10, 2, 3},
            {3, 3, 4, 8}
        };

        int[][] b4x4 = new int[][] {
            {12, 1, 4, 9},
            {9, 10, 12, 2},
            {3, 12, 4, 10}
        };

        // Act
        MatrixMultiply.DivNCnq.multiply(a4x4, b4x4, 0, 0, 0, 0, 4);
    }

    @Test
    public void testStrassenMultiplication() {
        // Arrange
        int[][] a4x4 = new int[][] {
            {5, 7, 9, 10},
            {2, 3, 3, 8},
            {8, 10, 2, 3},
            {3, 3, 4, 8}
        };

        int[][] b4x4 = new int[][] {
            {3, 10, 12, 18},
            {12, 1, 4, 9},
            {9, 10, 12, 2},
            {3, 12, 4, 10}
        };

        int[][] c4x4 = new int[][] {
            {210, 267, 236, 271},
            {93, 149, 104, 149},
            {171, 146, 172, 268},
            {105, 169, 128, 169}
        };

        // Act
        int[][] actual4x4 = MatrixMultiply.Strassen.multiply(a4x4, b4x4);

        // Assert
        assertTrue(Arrays.deepEquals(c4x4, actual4x4));
    }
}
