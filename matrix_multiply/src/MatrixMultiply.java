import java.util.Arrays;

class MatrixMultiply {

    static class Strassen {

        static int[][] multiply(int[][] a, int[][] b) {
            if (a.length != b.length)
                throw new IllegalArgumentException("Matrices dimensions must be equal");

            int n = a.length;
            int[][] res = new int[n][n];

            if (n == 1) {
                res[0][0] = a[0][0] * b[0][0];
            } else {

                // Dividing matrices into n/2 sub-matrices of 4
                int[][] a11 = new int[n/2][n/2];
                int[][] a12 = new int[n/2][n/2];
                int[][] a21 = new int[n/2][n/2];
                int[][] a22 = new int[n/2][n/2];

                int[][] b11 = new int[n/2][n/2];
                int[][] b12 = new int[n/2][n/2];
                int[][] b21 = new int[n/2][n/2];
                int[][] b22 = new int[n/2][n/2];

                // Copy data in a[1-4], b[1-4]
                divide(a11, a, 0, 0);
                divide(a12, a, 0, n/2);
                divide(a21, a, n/2, 0);
                divide(a22, a, n/2, n/2);

                divide(b11, b, 0, 0);
                divide(b12, b, 0, n/2);
                divide(b21, b, n/2, 0);
                divide(b22, b, n/2, n/2);

                // Create 10 sub matrices S[1-10], each n/2 and is sum of different matrices in prev step
                int[][] s1 = sub(b12, b22);
                int[][] s2 = add(a11, a12);
                int[][] s3 = add(a21, a22);
                int[][] s4 = sub(b21, b11);
                int[][] s5 = add(a11, a22);
                int[][] s6 = add(b11, b22);
                int[][] s7 = sub(a12, a22);
                int[][] s8 = add(b21, b22);
                int[][] s9 = sub(a11, a21);
                int[][] s10 = add(b11, b12);

                // Using sub matrices a[1-4],b[1-4],c[1-4] and S[1-10] - compute 7 matrix products P[1-7], each n/2
                int[][] p1 = multiply(a11, s1);
                int[][] p2 = multiply(s2, b22);
                int[][] p3 = multiply(s3, b11);
                int[][] p4 = multiply(a22, s4);
                int[][] p5 = multiply(s5, s6);
                int[][] p6 = multiply(s7, s8);
                int[][] p7 = multiply(s9, s10);

                // Compute c[1-4] by adding and substracting various combinations of P[1-10]
                int[][] c11 = add(sub(add(p5, p4), p2), p6);
                int[][] c12 = add(p1, p2);
                int[][] c21 = add(p3, p4);
                int[][] c22 = sub(sub(add(p5, p1), p3), p7);

                // Combine c[1-4] with res array
                copy(res, c11, 0, 0);
                copy(res, c12, 0, n/2);
                copy(res, c21, n/2, 0);
                copy(res, c22, n/2,n/2);
            }

            return res;
        }

        static int[][] sub(int[][] a, int[][] b) {
            int n = a.length;
            int[][] c = new int[n][n];

            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    c[i][j] = a[i][j] - b[i][j];

            return c;
        }

        static int[][] add(int[][] a, int[][] b) {
            int n = a.length;
            int[][] c = new int[n][n];

            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    c[i][j] = a[i][j] + b[i][j];

            return c;
        }

        // Assumes input matrices are square
        // Divides array by copying from a larger matrix into a smaller matrix
        // d is smaller matrix
        // s is bigger matrix, hence indexing is limited by smaller matrix d
        static void divide(int[][] d, int[][] s, int rowOffset, int colOffset) {
            for (int i1 = 0, i2 = rowOffset; i1 < d.length; i1++, i2++)
                for (int j1 = 0, j2 = colOffset; j1 < d.length; j1++, j2++)
                    d[i1][j1] = s[i2][j2];
        }

        // Assumes input matrices are square
        // Copies elements from a smaller matrix into a bigger one
        // d is bigger matrix
        // s is smaller matrix, hence indexing is limited by smaller matrix s
        static void copy(int[][] d, int[][] s, int rowOffset, int colOffset) {
            for (int i1 = 0, i2 = rowOffset; i1 < s.length; i1++, i2++)
                for (int j1 = 0, j2 = colOffset; j1 < s.length; j1++, j2++)
                    d[i2][j2] = s[i1][j1];
        }

    }

    static class DivNCnq {

        static int[][] multiply(int[][] a, int[][] b, int rowa, int cola, int rowb, int colb, int size) {
            if (a.length != b.length)
                throw new IllegalArgumentException("Matrices dimensions must be equal");

            int[][] c = new int[size][size];

            if (size == 1)
                c[0][0] = a[rowa][cola] * b[rowb][colb];
            else {
                int newSize = size / 2;

                sumMatrix(c,
                        multiply(a, b, rowa, cola, rowb, colb, newSize),
                        multiply(a, b, rowa, cola + newSize, rowb + newSize, colb, newSize),
                        0,0);

                sumMatrix(c,
                        multiply(a, b, rowa, cola, rowb, colb + newSize, newSize),
                        multiply(a, b, rowa, cola + newSize, rowb + newSize, colb + newSize, newSize),
                        0, newSize);

                sumMatrix(c,
                        multiply(a, b, rowa + newSize, cola, rowb, colb, newSize),
                        multiply(a, b, rowa + newSize, cola + newSize, rowb + newSize, colb, newSize),
                        newSize,0);

                sumMatrix(c,
                        multiply(a, b, rowa + newSize, cola, rowb, colb + newSize, newSize),
                        multiply(a, b, rowa + newSize, cola + newSize, rowb + newSize, colb + newSize, newSize),
                        newSize, newSize);
            }

            return c;
        }

        static void sumMatrix(int[][] c, int[][] a, int[][] b, int rowc, int colc) {
            int n = a.length;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    c[rowc + i][colc + j] = a[i][j] + b[i][j];
        }
    }

    static class Cubic {
        static int[][] multiply(int[][] a, int[][] b) {
            if (a == null || b == null)
                return new int[][]{};

            int n = Helpers.maxMatrixDimension(a, b);
            int[][] c = new int[n][n];
            Helpers.fillZeroes(c);

            for (int i = 0; i < n; ++i) {
                for (int j = 0; j < n; ++j) {
                    int sum = 0;
                    for (int k = 0; k < n; ++k)
                        sum += a[i][k] * b[k][j];
                    c[i][j] = sum;
                }
            }

            return c;
        }
    }



    public static void main(String[] args) {
        System.out.println("Matrix multiplication module");
    }
}
